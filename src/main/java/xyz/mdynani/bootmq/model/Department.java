package xyz.mdynani.bootmq.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Department
{
    private Integer DeptId;
    private String DeptName;
    private Integer NoOfEmployees;
}
