package xyz.mdynani.bootmq.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Employee
{
    private String firstName;
    private String lastName;
    private Long salary;
    private Date dateOfBirth;
    private Integer Age;
}
