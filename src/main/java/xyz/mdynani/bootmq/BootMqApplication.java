package xyz.mdynani.bootmq;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import xyz.mdynani.bootmq.service.ProducerService;

@SpringBootApplication
@EnableJms
public class BootMqApplication
{

    public static void main(String[] args) throws JsonProcessingException
    {
        ApplicationContext applicationContext = SpringApplication.run(BootMqApplication.class, args);
        /*JmsTemplate jmsTemplate = applicationContext.getBean(JmsTemplate.class);
        jmsTemplate.convertAndSend("javainuse", "test message");*/

        ProducerService producerService = applicationContext.getBean(ProducerService.class);
        producerService.sendToQueue();
        producerService.sendToTopic();
    }

}
